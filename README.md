# python像素贪吃蛇

#### 介绍
python像素贪吃蛇小游戏，可以通过上下左右键控制蛇头转向,点击回车键开始游戏。简单好玩

#### 软件架构
基于python3.0以上版本
基于pygame模块开发

#### 安装教程

1.  基于python3.0以上版本开发.开发时使用的是python3.7版本。

#### 使用说明

1.  基于python3.0以上版本开发，开发时使用的是python3.7版本。建议开发前本地安装pygame/random/sys模块
2.  用pycharm打开源文件（一般pycharm会自动提示需要安装的插件或者模块）
3.  点击retroSnaker.py,直接运行即可
#### 游戏截图
1. 游戏开始
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/095130_ba7c8abf_374985.png "1.png")
2. 游戏中
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/095144_9914b00f_374985.png "QQ截图20200715095030.png")
3. 游戏结束
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/095152_5f5089fd_374985.png "QQ截图20200715094955.png")

#### 参与贡献

1.  公众号：程序源代码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/090228_6a991c50_374985.jpeg "扫码_搜索联合传播样式-白色版.jpg")